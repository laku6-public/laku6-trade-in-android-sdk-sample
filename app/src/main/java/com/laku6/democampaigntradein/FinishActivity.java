package com.laku6.democampaigntradein;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.TextView;

public class FinishActivity extends AppCompatActivity {

    private TextView txtResult;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_finish);

        try {
            getSupportActionBar().setTitle("Finish");
        } catch (Exception e) {

        }

        String resultText = getIntent().getStringExtra("RESULT_TEXT");
        txtResult = findViewById(R.id.txtResult);
        txtResult.setText(resultText);
    }

    @Override
    public void onBackPressed() {

    }
}

