package com.laku6.democampaigntradein;

import android.Manifest;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.laku6.tradeinsdk.api.Laku6TradeIn;

import org.json.JSONObject;

public class MainActivity extends AppCompatActivity {
    private Laku6TradeIn laku6TradeIn;

    private final int MY_PERMISSIONS_REQUEST_READ_PHONE_STATE = 123;
    private final String TAG = "MAIN_ACTIVITY";

    private TextView txtDesc;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        getSupportActionBar().setTitle("Parent Activity");
        laku6TradeIn = laku6TradeIn.getInstance(this);
        laku6TradeIn.setCampaignTradeInId("campaignId12345");
        txtDesc = findViewById(R.id.txt_desc);

        final Button btnCustom = findViewById(R.id.button);
        btnCustom.setText("Launch Laku6 Library");
        btnCustom.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                laku6TradeIn.startGUITest();
            }
        });

        LocalBroadcastManager.getInstance(this).registerReceiver(mTestEndReceiver, new IntentFilter("laku6-test-end"));
        LocalBroadcastManager.getInstance(this).registerReceiver(mBackReceiver, new IntentFilter("laku6-back-action"));

        requestPermission();
    }

    private void requestPermission() {
        Log.d(TAG, "requestPermission()");
        if (!laku6TradeIn.permissionGranted()) {
            Log.d(TAG, "permissionNotGranted");
            ActivityCompat.requestPermissions(this,
                    new String[]{Manifest.permission.READ_PHONE_STATE},
                    MY_PERMISSIONS_REQUEST_READ_PHONE_STATE);
        } else {
            getMaxPrice();
        }
    }

    private void getMaxPrice() {
        Toast.makeText(this, "Calling Max Price API", Toast.LENGTH_SHORT).show();
        laku6TradeIn.getMinMaxPrice(new Laku6TradeIn.TradeInListener() {
            @Override
            public void onFinished(JSONObject result) {
                Log.d(TAG, result.toString());
                txtDesc.setText(result.toString());
            }

            @Override
            public void onError(JSONObject error) {
                Log.d(TAG, error.toString());
                txtDesc.setText(error.toString());
            }
        });
    }


    // Our handler for received Intents. This will be called whenever an Intent with an action named "laku6-test-end" is broadcasted.
    private BroadcastReceiver mTestEndReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            // Get extra data included in the Intent
            String message = intent.getStringExtra("test-result");
            Log.d("receiver", "Got message: " + message);

            Intent newIntent = new Intent(getBaseContext(), FinishActivity.class);
            newIntent.putExtra("RESULT_TEXT", message);
            startActivity(newIntent);
        }
    };

    private BroadcastReceiver mBackReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            // Get extra data included in the Intent. DO BACK TO PARENT
            Log.d("receiver", "Do back action to parent");
        }
    };

    @Override
    protected void onDestroy() {
        // Unregister since the activity is about to be closed.
        LocalBroadcastManager.getInstance(this).unregisterReceiver(mTestEndReceiver);
        LocalBroadcastManager.getInstance(this).unregisterReceiver(mBackReceiver);
        super.onDestroy();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST_READ_PHONE_STATE: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    getMaxPrice();
                } else {
                    requestPermission();
                }
                return;
            }
        }
    }
}
